import React from 'react';
import ReactDOM from 'react-dom';
import Basket from './Basket';

ReactDOM.render(
  <React.StrictMode>
    <Basket/>
  </React.StrictMode>,
  document.getElementById('root')
);
