import React from "react";
import Product from './Product';

const products = []
products[2] = {
    productId: 2,        
    name:"fortnite kreeft", 
    quantity: 1, 
    price:600
};
products[5] = {     
    productId: 5,      
    name:"gebaken iets", 
    quantity: 1, 
    price:600
};

export default function Menu({addToBasket}) {
    return (
        products.map(product => {
            return <Product key = {product.productId} addToBasket = {addToBasket} product={product}/>
        })
    )
}
