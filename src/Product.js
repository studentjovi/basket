import React from 'react'

export default function Product({product, addToBasket}) {
    function handleProductClick(e){
        addToBasket(product)
    }
    return (
        <div>
            <button onClick = {handleProductClick}>{product.name} {product.price}</button>
        </div>
    )
}
